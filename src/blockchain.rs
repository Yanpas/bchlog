extern crate regex;
extern crate openssl;
extern crate base64;
extern crate chrono;

use std;
use std::io;
use std::io::Write;
use std::fs;
use std::collections;
use std::collections::LinkedList;

use self::chrono::offset::TimeZone;
use self::openssl::hash::{Hasher, MessageDigest, DigestBytes};

type HashSum = DigestBytes;

const HASH_LEN: usize = 32;
const KEY_LEN: usize = 256/8;
const FIRST_MESSAGE: &str = "First message in block";
const TIME_FORMAT: &str = "%Y-%m-%dT%H:%M:%S";

const HASHDIGEST: fn() -> MessageDigest = MessageDigest::sha256;

#[derive(Debug)]
pub enum Error {
	InputError(String),
	BlockChainError(String)
}

use self::Error::{InputError, BlockChainError};
use std::error::Error as StdErr;

impl From<openssl::error::ErrorStack> for Error {
	fn from(e: openssl::error::ErrorStack) -> Self {
		Error::InputError(format!("{}", e))
	}
}

impl std::fmt::Display for Error {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
		write!(f, "{}", self.description())
	}
}

impl std::error::Error for Error {
	fn description(&self) -> &str {
		match self {
			&InputError(ref s)=> &s,
			&BlockChainError(ref s)=> &s
		}
	}
}

pub struct Entry {
	pub time: chrono::DateTime<chrono::Utc>,
	pub svcname: String,
	pub payload: String
}

struct Block(Vec<Entry>);

impl Block {
	fn new() -> Self {
		Block (Vec::<Entry>::new())
	}
}

pub struct BlockChain {
	prev_blocks: LinkedList<Block>,
	file: Option<fs::File>,
	path: String,
	name: String,
	password: [u8; KEY_LEN],
	counter: u64, // last number of an entry in the whole chain
	curr_hasher: Hasher
}

pub struct Iter<'a> {
	it_outer: collections::linked_list::Iter<'a, Block>,
	it_inner: Option<std::slice::Iter<'a, Entry>>,
	block_no: i32
}

fn get_entry_hash<'a>(n: &'a str, t: &'a str, svc: &'a str, msg: &'a str) -> HashSum {
	let mut h = Hasher::new(HASHDIGEST()).unwrap();
	[n,t,svc,msg].iter().for_each(|s|h.update(s.as_bytes()).unwrap());
	return h.finish2().unwrap();
}

impl BlockChain {
	pub fn open(path: &str, name: &str, pw: &str, init: &[u8]) -> Result<BlockChain, Error> {
		let mut password = [0u8; KEY_LEN];
		for (i, pw_byte) in pw.as_bytes().iter().enumerate() {
			if i >= KEY_LEN {
				return Err(InputError("Password is too long".to_string()));
			}
			password[i] = *pw_byte;
		}
		let mut bch = BlockChain {
			prev_blocks: LinkedList::<Block>::new(),
			file: None,
			path: String::from(path),
			name: String::from(name),
			password,
			counter: 0u64,
			curr_hasher: Hasher::new(HASHDIGEST()).unwrap()
		};
		bch.curr_hasher.update(init).unwrap();
		let dir = fs::read_dir(path).map_err(|e|InputError(format!("Failed to open log dir {}: {}", path, e.description())))?;
		let mut paths = Vec::<(String, u32)>::new();
		let rxp = regex::Regex::new(&(name.to_string() + r"\.(\d+)")).expect("Journal name regex");
		for item_res in dir {
			let item = item_res.unwrap();
			let itemname = item.path().to_str().unwrap().to_string();
			if item.file_type().unwrap().is_file() {
				if let Some(capts) = rxp.captures(&itemname) {
					let num = capts.get(1).unwrap().as_str().parse::<u32>().unwrap();
					paths.push((itemname.clone(), num));
				} else {
					warn!("Extraneous file in log dir {}", itemname);
				}
			} else {
				warn!("Unknown entry in log dir {}", itemname);
			}
		}
		let mut current_block_n = 1;
		if !paths.is_empty() {
			paths.sort_by_key(|&(_, e)| e);
			for &(ref path, block_n) in &paths {
				if current_block_n != block_n {
					return Err(BlockChainError(format!("Missing file for block {}", current_block_n)))
				}
				debug!("Parsing file {}", path);
				bch.add_block(&path)?;
				current_block_n += 1;
			}
		}
		return Ok(bch);
	}

	fn add_block(&mut self, path: &str) -> Result<(), Error> {
		let file = fs::File::open(&path).unwrap();
		let mut reader = io::BufReader::new(file);
		let mut firstline = String::new();
		io::BufRead::read_line(& mut reader, & mut firstline)
			.map_err(|e|BlockChainError(format!("Failed to read 1st line in {} : {}", path, e)))?;
		if firstline.len() == 1 {
			return Err(BlockChainError("First line is too short in ".to_string() + path))
		}
		firstline.pop();
		let hashbytes = base64::decode(&firstline)
			.map_err(|_| BlockChainError("Hash base64 decode error in the header of ".to_string() + path) )?;
		if hashbytes.len() != HASH_LEN {
			return Err(BlockChainError(format!("Wrong hash length in {} : {}", path, hashbytes.len())));
		}
		let prev_hash = self.curr_hasher.finish2().unwrap();
		if *hashbytes != *prev_hash {
			return Err(BlockChainError(format!("Hash mismatch for block {}", path)));
		}
		let mut res = Block::new();
		const FIELDS_IN_LINE: usize = 5;
		let mut this_block_hasher = Hasher::new(HASHDIGEST()).unwrap();
		for (i, line_res) in io::BufRead::lines(& mut reader).enumerate() {
			let line = line_res.unwrap();
			let fields : Vec<&str> = line.splitn(FIELDS_IN_LINE, |x: char|x == ' ').collect();
			let lineno = i+1;
			trace!("Reading line {}", lineno);
			if fields.len() != FIELDS_IN_LINE {
				return Err(BlockChainError(format!("Invalid number of fields in {}:{}", path, lineno)));
			}

			let number = fields[0];
			let time = fields[1];
			let svcname = fields[2].to_string();
			let msg = fields[4];

			this_block_hasher.update(number.as_bytes()).unwrap();
			this_block_hasher.update(time.as_bytes()).unwrap();
			this_block_hasher.update(svcname.as_bytes()).unwrap();
			this_block_hasher.update(msg.as_bytes()).unwrap();

			// parse fields
			let number = number.parse::<u64>()
				.map_err(|_|BlockChainError(format!("Field 1 not a number in {}:{}", path, lineno)))?;
			let time = chrono::Utc.datetime_from_str(time, TIME_FORMAT)
				.map_err(|e|BlockChainError(format!("Not ISO time in {}:{} ({}) : {}", path, lineno, fields[1], e)))?;
			let digisign = base64::decode(fields[3])
				.map_err(|_|BlockChainError(format!("Base64 digisign decode err in {}:{}", path, lineno)))?;

			// validate number
			self.counter += 1;
			if self.counter != number {
				return Err(BlockChainError(format!("Sequence number error for {}:{}", path, lineno)));
			}

			// validate signature
			let signhash1 = openssl::symm::decrypt(openssl::symm::Cipher::aes_256_ecb(),
					&self.password, None, digisign.as_slice())
				.map_err(|e|BlockChainError(format!("Decryption error in entry '{}:{}': {}", path, lineno, e.description())))?;
			let signhash2 = get_entry_hash(fields[0], fields[1], &svcname, msg);
			if *signhash1.as_slice() != *signhash2 {
				return Err(BlockChainError(format!("Signature validation failure in {}:{}", path, lineno)));
			}
			res.0.push(Entry{svcname, time, payload: msg.to_string()})
		}
		if res.0.is_empty() {
			return Err(BlockChainError(format!("Block {} is empty", path)));
		}
		self.curr_hasher = this_block_hasher;
		self.prev_blocks.push_back(res);
		return Ok(());
	}

	pub fn iter(&self) -> Iter {
		Iter {
			it_outer: self.prev_blocks.iter(),
			it_inner: None,
			block_no: 0
		}
	}

	pub fn switch_to_new_block(&mut self) -> Result<(), io::Error> {
		let new_block_n = (self.prev_blocks.len() + 1).to_string();
		let path: std::path::PathBuf = [self.path.clone(), self.name.clone() + "." + &new_block_n].iter().collect();
		self.file = Some(fs::File::create(path)?);
		self.prev_blocks.push_back(Block::new());
		let prev_hash = self.curr_hasher.finish2().unwrap();
		writeln!(self.file.as_ref().unwrap(), "{}", base64::encode(&prev_hash))?;
		info!("New block ({}) has been created", self.prev_blocks.len());
		self.new_entry("bchlog", FIRST_MESSAGE)?;
		Ok(())
	}

	pub fn new_entry(&mut self, svc_name: &str, message: &str) -> Result<(), io::Error> {
		debug!("New entry in log from {}: {}", svc_name, message);
		self.counter += 1;
		let t = chrono::Utc::now();
		self.prev_blocks.back_mut().expect("No blocks in journal!").0.push(Entry {
			time: t.clone(),
			svcname: svc_name.to_string(),
			payload: message.to_string()
		});
		let t = t.format(TIME_FORMAT).to_string();
		let entry_no = self.counter.to_string();

		self.curr_hasher.update(entry_no.as_bytes()).unwrap();
		self.curr_hasher.update(t.as_bytes()).unwrap();
		self.curr_hasher.update(svc_name.as_bytes()).unwrap();
		self.curr_hasher.update(message.as_bytes()).unwrap();

		let sig_hash = get_entry_hash(&entry_no, &t, svc_name, message);
		let sign = openssl::symm::encrypt(openssl::symm::Cipher::aes_256_ecb(), &self.password, None, &sig_hash)?;
		let sign = base64::encode(&sign);
		writeln!(self.file.as_ref().unwrap(), "{n} {t} {svc} {sig} {msg}", n=self.counter, t=t, svc=svc_name, sig=sign, msg=message)?;

		Ok(())
	}
}

impl<'a> Iterator for Iter<'a> {
	type Item = (i32, &'a Entry);
	fn next(&mut self) -> Option<Self::Item> {
		let it = match self.it_inner {
			Some(ref mut it) => match it.next() {
				Some(e) => return Some((self.block_no, e)),
				None => match self.it_outer.next() {
						Some(block) => {
							block.0.iter()
						},
						None => return None
					}
				}
			None => match self.it_outer.next() {
				Some(block) => {
					block.0.iter()
				},
				None => return None
			}
		};
		self.it_inner = Some(it);
		self.block_no += 1;
		self.next()
	}
}
