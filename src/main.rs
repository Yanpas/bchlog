#[macro_use] extern crate log;
extern crate env_logger;
extern crate ini;

mod blockchain;
mod server;
mod syslog;

use std::process::{exit};
use std::error::Error;
use blockchain::BlockChain;

fn main() {
	env_logger::init();
	let args : Vec<String> = std::env::args().collect();
	let conf = ini::Ini::load_from_file(if args.len() > 1 {&args[1]} else {"bchlog.conf"})
		.map_err(|e|{eprintln!("Failed to parse config file: {}", e.description()); exit(1)}).unwrap();
	let setts = conf.section(Some("bchlog"))
		.unwrap_or_else(||{eprintln!("No bchlog section in config file"); exit(1)});
	for val in ["path", "logname", "password", "init"].iter() {
		if !setts.contains_key(*val) {
			eprintln!("No {} property in configuration file", val);
			exit(1);
		}
	}
	let bch = BlockChain::open(&setts["path"], &setts["logname"], &setts["password"], setts["init"].as_bytes())
		.map_err(|e|{println!("Failed to open log: {}", e.description()); exit(1)}).unwrap();
	debug!("Log was opened successfully");
	if args.get(2).map(|e| e == "show").unwrap_or(false) {
		let mut entry_no = 0;
		let mut currentblock_no = 0;
		for (block_no, entry) in bch.iter() {
			if currentblock_no != block_no {
				currentblock_no = block_no;
				entry_no = 1;
			}
			println!("{}.{} [{:<10} {}]: {}", block_no, entry_no, entry.svcname, entry.time, entry.payload);
			entry_no += 1;
		}
		return;
	}

	let mut ipaddr = None;
	if setts.contains_key("ipaddr") {
		ipaddr = Some(setts["ipaddr"].parse().unwrap_or_else(|_|{
			eprintln!("Failed to parse ip address from config");
			exit(1)
		}));
	}
	let switch_to : u64 = if setts.contains_key("switchtime") {
		setts["switchtime"].parse().unwrap()
	} else {
		600
	};
	server::start(bch, ipaddr, setts.get("unixaddr"), switch_to);
}
