extern crate futures;
extern crate tokio_core;
extern crate tokio_io;
extern crate tokio_signal;
extern crate tokio_timer;
#[cfg(unix)] extern crate tokio_uds;

use std;
use std::cell::RefCell;
use std::net::SocketAddr;
use std::process::exit;
use std::rc::Rc;
use std::time::Duration;

use self::futures::{Future, Stream};
use self::tokio_core::reactor::{Core};
use self::tokio_io::{AsyncRead};
use self::tokio_timer::Timer;

use blockchain::BlockChain;
use syslog;

/// timeout in seconds
fn init_time(core: &Core, bchrc: Rc<RefCell<BlockChain>>, timeout: u64) {
	let handle = core.handle();
	let timer = Timer::default().interval(Duration::from_secs(timeout)).for_each(move |_|{
		bchrc.borrow_mut().switch_to_new_block().expect("Failed to switch to new block");
		let newtimer = tokio_core::reactor::Timeout::new(Duration::from_secs(timeout), &handle).unwrap().map_err(|_|());
		handle.spawn(newtimer);
		Ok(())
	}).map_err(|_|());
	core.handle().spawn(timer);
}

fn handle_new_message(bch: &mut BlockChain, msg: &syslog::LogMessage) {
	debug!("Received msg {{ (facility {}), (severity {:?}) (app {:?}) }}: {}",
		msg.facility, msg.severity, msg.app, msg.msg);
	let appname = msg.app.as_ref().map(|s|s.as_ref()).unwrap_or("?");
	if appname.chars().any(|e| e == ' ' || e.is_control()) {
		info!("Invalid appname in message, skipping");
		return;
	}
	if msg.msg.chars().any(|e| e.is_control()) {
		info!("Invalid payload in message, skipping");
		return;
	}
	bch.new_entry(appname, &msg.msg).expect("new entry");
}

macro_rules! handle_conn {
	($listener:expr, $handle:expr, $bchrc:expr) => {
		let handle2 = $handle.clone();
		let listen_stream = $listener.incoming().for_each(move |(cli, addr)|{
			debug!("Connection from {:?}", addr);
			let handle3 = handle2.clone();
			let bchrc = $bchrc.clone();
			handle2.spawn_fn(move || {
				let requests = cli.framed(syslog::Syslog::new());
				let bchrc = bchrc.clone();
				handle3.spawn(requests.into_future()
					.map(move |(maybemsg, _framed)|{
						if let Some(msg) = maybemsg {
							handle_new_message(&mut (*bchrc).borrow_mut(), &msg);
						} else {
							debug!("Msg is None!")
						}
						()
					})
					.map_err(|(e, _framed)| {
						info!("Failed to parse syslog: {}", e);
						()
				}));
				Ok(())
			});
			Ok(())
		}).map_err(|_|());
		$handle.spawn(listen_stream);
	};
}

pub fn start(mut bch: BlockChain, ip_addr: Option<SocketAddr>, unix_addr: Option<&String>, switch_timeout: u64) -> ! {
	if ip_addr.is_none() && unix_addr.is_none() {
		eprintln!("No listen address specified in config");
		exit(1);
	}
	bch.switch_to_new_block().unwrap();

	let mut core = Core::new().expect("core new");
	let bchrc = Rc::new(RefCell::new(bch));
	init_time(&core, bchrc.clone(), switch_timeout);

	let sigint = tokio_signal::ctrl_c(&core.handle()).flatten_stream().take(1).for_each(|()|{
		info!("Received sigint, stopping");
		Ok(())
	});

	if let Some(ip_addr) = ip_addr {
		let bchrc = bchrc.clone();
		let handle = core.handle();
		let tcp = tokio_core::net::TcpListener::bind(&ip_addr, &handle).expect("failed to bind tcp");
		handle_conn!(tcp, handle, bchrc);
	}

	#[cfg(unix)] {
		if let Some(sockpath) = unix_addr {
			let bchrc = bchrc.clone();
			let handle = core.handle();
			let sockpath = std::path::Path::new(sockpath);
			if sockpath.exists() {
				std::fs::remove_file(sockpath).expect("Failed to remove socket path");
			}
			let uds = tokio_uds::UnixListener::bind(sockpath, &core.handle())
				.map_err(|e|{error!("Unix bind error {}", e); e})
				.expect("failed to bind unix");
			handle_conn!(uds, handle, bchrc);
		}
	}

	core.run(sigint).expect("core run error");
	info!("Server stopped");
	exit(0);
}