extern crate tokio_proto;
extern crate tokio_io;
extern crate bytes;
extern crate regex;

use std;
use std::io;
use std::str;
use self::bytes::BytesMut;

#[derive(Clone)]
pub struct Syslog {
	regex: regex::Regex
}

#[allow(dead_code)]
#[derive(Debug)]
#[repr(i32)]
pub enum Severity {
	Emergency,
	Alert,
	Critical,
	Error,
	Warning,
	Notice,
	Informational,
	Debug
}

impl Severity {
	fn from_int(i: u32) -> Option<Severity> {
		return match i {
			0 ... 7 => Some(unsafe {std::mem::transmute::<u32, Severity>(i)}),
			_ => None
		}
	}
}

pub struct LogMessage {
	pub severity: Severity,
	pub facility: u32,
	pub app: Option<String>,
	pub msg: String
}

const SYSLOG_RXP: &str = r"(?P<pri>\d{1,3})>(?P<ver>.*?) (?P<tm>.*?) (?P<host>.*?) (?P<app>.*?) (?P<proc>.*?) (?P<msgid>.*?) (?:-|\[.*?\]) (?P<msg>.*)";

impl Syslog {
	pub fn new() -> Syslog {
		Syslog {
			regex: regex::Regex::new(SYSLOG_RXP).expect("Syslog regex")
		}
	}
}

impl tokio_io::codec::Decoder for Syslog {
	type Item = LogMessage;
	type Error = io::Error;
	fn decode(&mut self, buf: &mut BytesMut) -> io::Result<Option<LogMessage>> {
		if let Some(i) = buf.iter().position(|&b| b == b'\n') {
			let decode_err = |text: &str| io::Error::new(io::ErrorKind::Other, text);
			let linebuf = buf.split_to(i);
			buf.split_to(1); // remove '\n'
			let linetext = str::from_utf8(&linebuf).map_err(|_|decode_err("UTF-8 decode error"))?;
			trace!("Received from socket: '{}'", linetext);
			let regmatch = self.regex.captures(linetext).ok_or(decode_err("String doesn't match syslog regex"))?;
			let pri: u32 = regmatch.name("pri").unwrap().as_str().parse().map_err(|_|decode_err("Prio is not int"))?;
			let facility = pri / 8;
			let severity = Severity::from_int(pri % 8).unwrap();
			let appname = regmatch.name("app").unwrap().as_str().to_owned();
			return Ok(Some(LogMessage {
				severity,
				facility,
				app: if appname == "-" {None} else {Some(appname)},
				msg: regmatch.name("msg").unwrap().as_str().to_owned()
			}));
		}
		else {
			return Ok(None);
		}
	}
}

impl tokio_io::codec::Encoder for Syslog {
	type Item = LogMessage;
	type Error = io::Error;
	fn encode(&mut self, item: Self::Item, dst: &mut BytesMut) -> Result<(), Self::Error> {
		let result = format!("<{}>- - - - - - - {}\n", (item.facility*8 + item.severity as u32), item.msg);
		dst.extend_from_slice(result.as_bytes());
		Ok(())
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn regex_test() {
		let sysl = Syslog::new();
		let rmatch = sysl.regex.captures("<2>4 - - - - - [] msg").unwrap();
		assert!(rmatch.name("pri").unwrap().as_str() == "2");
		assert!(rmatch.name("msg").unwrap().as_str() == "msg");
	}
}
