import matplotlib.pyplot as plt
import numpy as np

xs = [4000, 3800, 3600, 3400, 3200, 2100 ,2300]
ys = [388, 356, 326, 296, 268, 134, 156]

sum_x = sum(xs)
sum_y = sum(ys)
sum_x2 = sum(x**2 for x in xs)
sum_xy = sum(x*y for (x,y) in zip(xs,ys))
# print(sum_x, sum_y, sum_x2, sum_xy)

matrix = np.array([[sum_x2, sum_x], [sum_x, len(xs)]]) # левая часть системы
vector = np.array([sum_xy, sum_y]) # правая часть системы

a,b = np.linalg.solve(matrix, vector)

plt.plot(xs, ys, '.')
plt.plot([2000,4000], [a*x + b for x in [2000,4000]])
plt.xlabel('Частота ЦПУ (МГц)')
plt.ylabel('Количество событий в секунду')
plt.show()