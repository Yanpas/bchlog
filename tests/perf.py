#/usr/bin/python

import socket
import sys

MSG = b'<2>100 2017-06-05T20:01:12 example.com kernel - 123 - testmsg'

times = 1000

if len(sys.argv) <= 1:
	exit('1 arg required')

if len(sys.argv) > 2:
	times = int(sys.argv[2])

portno = int(sys.argv[1])
while times:
	times -= 1
	with socket.socket() as s:
		s.connect(('127.0.0.1', portno))
		s.send(MSG + b"\n")
