import matplotlib.pyplot as plt

def func(x):
	return 0.1017 * x - 20

def realF(x):
	return 0.000015 * x**2 + 0.042* x - 20

xs = [4000, 3800, 3600, 3400, 3200, 2100 ,2300]
ys = [realF(x) for x in xs]
ys2 = [round(realF(x)) for x in xs]
print (ys2)
plt.plot(xs, ys, '.')
plt.xlabel('Частота ЦПУ (МГц)')
plt.ylabel('Количество событий в секунду')
plt.show()